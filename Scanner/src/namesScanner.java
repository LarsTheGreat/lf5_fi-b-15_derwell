import java.util.Scanner; // Import der Klasse Scanner

public class namesScanner {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Geben Sie bitte Ihren Namen ein:");

		String name = myScanner.next();

		System.out.println("geben Sie bitte Ihr Alter an:");

		int alter = myScanner.nextInt();

		System.out.println("Ihr Name ist " + name);
		System.out.println("Sie sind " + alter + " Jahre alt");

		myScanner.close();
	}
}