import java.util.Scanner;

public class aufgabe3RoemischeZahlen {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine roemische Zahl an.");
		char roemischeZahl = tastatur.next().charAt(0);
		
		
		switch (roemischeZahl) {
			case 'I':
				System.out.println("I = 1");
				break;
			case 'V':
				System.out.println("V = 5");
				break;
			case 'X':
				System.out.println("X = 10");
				break;
			case 'L':
				System.out.println("L = 50");
				break;
			case 'C':
				System.out.println("C = 100");
				break;
			case 'D':
				System.out.println("D = 500");
				break;
			case 'M':
				System.out.println("M = 1000");
				break;
			default:
				System.out.println("Fehler, bitte geben Sie eine richtige roemische Zahl an.");
		}
	}

}
