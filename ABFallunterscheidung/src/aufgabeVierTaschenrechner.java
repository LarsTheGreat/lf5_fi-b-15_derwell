import java.util.Scanner;

public class aufgabeVierTaschenrechner {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double x;
		double y;
		double z;
		char rechenopperation;
		
		System.out.println("Geben Sie die Rechenopperation an, die Sie ausf�hren wollen.");
		rechenopperation = tastatur.next().charAt(0);
		
		System.out.println("Geben Sie die beiden Zahlen an die berechnen wollen.");
		try {
		x = tastatur.nextDouble();
		y = tastatur.nextDouble();
		System.out.print(x);
		System.out.print(y);
		
		
		switch (rechenopperation) {
			case '+':
				z = x + y;
				System.out.println("Ihr Ergebnis ist: " + z);
				break;
			case '-':	
				z = x - y;
				System.out.println("Ihr Ergebnis ist: " + z);
				break;
			case '*':
				z = x * y;
				System.out.println("Ihr Ergebnis ist: " + z);
				break;
			case '/':
				z = x / y;
				System.out.println("Ihr Ergebnis ist: " + z);
				break;
			default:
				System.out.println("Fehler, bitte einen richtigen Rechenopperator angeben.");
		} 
		}catch (Exception e) {
		      System.out.println("Something went wrong.");
		}
	}
}
