import java.util.Scanner;

public class aufgabe4 {

	public static void main(String[] args) {

		titel();
		String variableEins;
		variableEins = körper();
		// System.out.println(variableEins);
		// Eingabe (E) Abfrage welcher Körper berechnet werden soll

		if (variableEins.equals("Würfel")) {
			System.out.println(variableEins);
			System.out.println("\nEingabe Abmessungen Würfel");
			double kanteWuerfel = eingabe("Bitte die Kantenlänge des Würfels eingeben: ");
			double volumenQuader = berechnungWuerfel(kanteWuerfel);
			ausgabe("Quader", volumenQuader);

			
		}
		if (variableEins.equals("Quarder")) {
			System.out.println(variableEins);
			System.out.println("\nEingabe Abmessungen Quader");
			double hoeheQuader = eingabe("Bitte die Höhe des Quaders eingeben: ");
			double laengeQuader = eingabe("Bitte die Länge des Quaders eingeben: ");
			double breiteQuader = eingabe("Bitte die Breite des Quaders eingeben: ");
			double volumenQuader = berechnungQuader(hoeheQuader, breiteQuader, laengeQuader);
			ausgabe("Quader", volumenQuader);			
			
		}
		if (variableEins.equals("Pyramide")) {
			System.out.println(variableEins);
			System.out.println("\nEingabe Abmessungen Pyramide");
			double hoehePyramide = eingabe("Bitte die Höhe der Pyramide eingeben: ");
			double seitePyramide = eingabe("Bitte die Seitenlänge der Pyramide eingeben: ");
			
			double volumenPyramide = berechnungPyramide(seitePyramide, hoehePyramide);
			ausgabe("Pyramide", volumenPyramide);

		}
		if (variableEins.equals("Kugel")) {
			System.out.println(variableEins);
			System.out.println("\nEingabe Abmessungen Kugel");
			double radiusKugel = eingabe("Bitte den Radius der Kugel eingeben: \n\n");
			double volumenKugel = berechnungKugel(radiusKugel);
			ausgabe("Kugel", volumenKugel);
		}

	}

	public static String körper() {
		Scanner tastatur = new Scanner(System.in);
		String variableEins;
		System.out.print("Wollen Sie einen Würfel, Quarder, Pyramide oder Kugel berechnen: ");
		variableEins = tastatur.next();
		return variableEins;
	}

	public static String titel() {
		String titel = "Sie können entweder einen Würfel, Quarder, Pyramide oder Kugel berechnen\n";
		System.out.print(titel);
		return titel;
	}

	public static double eingabe(String anweisung) {
		// hier wird er scanner eingefügt von Lukas Test inspirieren lassen
		System.out.print(anweisung);
		Scanner scanner = new Scanner(System.in);
		return scanner.nextDouble();
	}
	

	public static double berechnungWuerfel(double a) {
		return a * a * a;
	}

	public static double berechnungQuader(double a, double b, double c) {
		return a * b * c;
	}

	public static double berechnungPyramide(double a, double h) {
		return a * a * h / 3;
	}

	public static double berechnungKugel(double r) {
		double pi = 3.14;
		return r * r * r * pi * 4 / 3;
	}

	public static void ausgabe(String koerper, double volumen) {
		System.out.println("Volumen " + koerper + ": " + volumen +"m");
	}

}

//a) Würfel: V = a * a * a
//b) Quader: V = a * b * c
//c) Pyramide: V = a * a * h / 3
//d) Kugel: V = 4/3 * r³ * π.