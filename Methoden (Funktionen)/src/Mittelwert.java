
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double a = 2.0;
      double b = 4.0;
      double m;
      m = berechneMittelwert(a,b);
      System.out.println(m);
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ==============================
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", a, b, m);
   }
   public static double berechneMittelwert( double x, double y) {
	   return (x + y) / 2.0;
	   
   }
}
