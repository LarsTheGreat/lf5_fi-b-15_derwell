import java.util.Scanner;
//von Lukas wie funktionieren Eingaben
public class test {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		titel();

		System.out.println("Eingabe Abmessungen W�rfel");
		double kanteWuerfel = eingabe("Bitte die Kantenl�nge des W�rfels eingeben: ");

		System.out.println("\nEingabe Abmessungen Quader");
		double hoeheQuader = eingabe("Bitte die H�he des Quaders eingeben: ");
		double laengeQuader = eingabe("Bitte die L�nge des Quaders eingeben: ");
		double breiteQuader = eingabe("Bitte die Breite des Quaders eingeben: ");

		System.out.println("\nEingabe Abmessungen Pyramide");
		double hoehePyramide = eingabe("Bitte die H�he der Pyramide eingeben: ");
		double seitePyramide = eingabe("Bitte die Seitenl�nge der Pyramide eingeben: ");

		System.out.println("\nEingabe Abmessungen Kugel");
		double radiusKugel = eingabe("Bitte den Radius der Kugel eingeben: \n\n");

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double volumenWuerfel = berechnungWuerfel(kanteWuerfel);
		double volumenQuader = berechnungQuader(hoeheQuader, breiteQuader, laengeQuader);
		double volumenPyramide = berechnungPyramide(seitePyramide, hoehePyramide);
		double volumenKugel = berechnungKugel(radiusKugel);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ausgabe("W�rfel", volumenWuerfel);
		ausgabe("Quader", volumenQuader);
		ausgabe("Pyramide", volumenPyramide);
		ausgabe("Kugel", volumenKugel);
	}

	public static double eingabe(String anweisung) {
		System.out.print(anweisung);
		Scanner scanner = new Scanner(System.in);
		return scanner.nextDouble();
	}

	public static void titel() {
		System.out.println("Dieses Programm berechnet Volumen verschiedener geometrischer K�rper");
		System.out.println("--------------------------------------------------------------------");
	}

	public static double berechnungWuerfel(double a) {
		return a * a * a;
	}

	public static double berechnungQuader(double a, double b, double c) {
		return a * b * c;
	}

	public static double berechnungPyramide(double a, double h) {
		return a * a * h / 3;
	}

	public static double berechnungKugel(double r) {
		double pi = 3.14;
		return r * r * r * pi * 4 / 3;
	}

	public static void ausgabe(String koerper, double volumen) {
		System.out.println("Volumen " + koerper + ": " + volumen);
	}

}