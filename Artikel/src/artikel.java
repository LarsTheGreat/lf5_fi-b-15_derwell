public class artikel {

	private String name;
	private int nummer;
	private double einkaufpreis;
	private double verkaufpreis;
	private int mindestbestand;
	private int lagerbestand;
	
	public artikel() {
	}
	
	public artikel(String name) {
		this.name = name;
	}
	
	public artikel (String name, int nummer, double einkaufpreis, double verkaufpreis, int mindestbestand, int lagerbestand) {
		this.name = name;
		this.nummer = nummer;
		this.einkaufpreis = einkaufpreis;
		this.verkaufpreis = verkaufpreis;
		this.mindestbestand = mindestbestand;
		this.lagerbestand = lagerbestand;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	public void setNummer(int nummer) {
		this.nummer = nummer;
	}
	public int getNummer () {
		return this.nummer;
	}
	public void setEinkaufpreis(double einkaufpreis) {
		this.einkaufpreis = einkaufpreis;
	}
	public double getEinkaufpreis () {
		return this.einkaufpreis;
	}
	public void setVerkaufpreis(double verkaufpreis) {
		this.verkaufpreis = verkaufpreis;
	}
	public double getVerkaufspreis() {
		return this.verkaufpreis;
	}
	public void setMindestbestand(int mindestbestand) {
		this.mindestbestand = mindestbestand;
	}
	public int getMindestbestand() {
		return this.mindestbestand;
	}
	public void setLagerbestand(int lagerbestand) {
		this.lagerbestand = lagerbestand;
	}
	public int getLagerbestand() {
		return this.lagerbestand;
	}
	public void bestelleArtikel() {
	}
	public void veraendereLagerbestend () {
	}
	public void berechneGewinn() {
	}
}
