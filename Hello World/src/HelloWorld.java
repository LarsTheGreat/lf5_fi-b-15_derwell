public class HelloWorld {

	public static void main(String[] args) {

		System.out.print("Das ist ein Beispielsatz");
		System.out.print(" ein Beispielsatz ist das \n");
		System.out.println("Das ist ein Beispielsatz \nein Beispielsatz ist das");
		//Das ist ein Kommentar
		
		System.out.println("   +   \n  +++  \n +++++ \n+++++++ \n   +   \n   +   ");
		System.out.println(" ");
		
		//System.out.printf(format, arguments);
		System.out.printf("Hello %s!%n", "World");
		System.out.println(" "); 
		
		
		String s = "Java-Programm";
		System.out.printf( "%.4s\n", s );
		
		String d = "22.4234234";
		System.out.printf( "%.5s\n", d );
		String f = "111.2222";
		System.out.printf("%.6s\n", f);
		
		double i = 4.0;
				
		System.out.printf( "%03.2f \n", i);
		
		double r = 1000000.551;
		
	}

}

