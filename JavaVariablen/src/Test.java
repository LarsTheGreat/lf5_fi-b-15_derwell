
public class Test {
		public static void main(String[] args) {
			
		
		String n = "Nils Derwell";
		String m ="nils";
		
		System.out.printf("|%s|", n);
		System.out.printf("\n|%20s|\n", n);
		System.out.printf("|%-20s|\n", n);
		
		System.out.printf("|%9s|\n", n);
		System.out.printf("|%-4s|\n", n);
		
		System.out.printf("|%20.4s|\n", n);
		
		int i = 234;
	
		
		System.out.printf("|%d| |%d|\n", i, -i);
		
		System.out.printf("|%-10d| |%-10d|\n", i, -i);		//linksbündig mit 10 Stellen
		System.out.printf( "|%10d| |%10d|\n" , i, -i);		//mit min 10 Stellenrechtsbündig
		System.out.printf("|%+-5d| |%+-5d|\n",i,-i);		//Vorzeichen (auch positiv
		System.out.printf("|%05d| |%05d|\n", i,-i);			//mit Nullen aufüllen
		System.out.printf("|%05d| |%05d|\n",i,-i);			
		
		double g = 123.123;
		
		System.out.printf("|%f| |%f|\n", g,-g);
		System.out.printf("|%.2f| |%10.2f|\n",g,-g);		//mit 2 nachkommastellen
		System.out.printf("|%10.2f| |%10.2f|\n",g,-g);		//10zeichen mit 2 nachkommastellen
		System.out.printf("|%-10f| |%-10f|\n",g,-g);		//10 Zeichen min.
		System.out.printf("|%010.2f||%010.2f|\n",g,-g);		//10 ZEichen mit Nullen aufgefüllt und 2 nachkommastellen
		
			//Aufgabe3
		double f = 22.4234234;
		System.out.printf("%.2f\n",f);
		double t = 111.2222;
		System.out.printf("%.2f\n",t);
		double r = 4.0;
		System.out.printf("%04.2f\n",r);
		
		
		
		
		
	}
}
