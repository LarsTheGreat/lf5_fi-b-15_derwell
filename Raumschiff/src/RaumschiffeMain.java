
public class RaumschiffeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Raumschiffe
		Raumschiff r1klingonen = new Raumschiff();
		Raumschiff r2romulaner = new Raumschiff();
		Raumschiff r3vulkanier = new Raumschiff();
		
		r1klingonen.setSchiffsname ("IKS Hegh`ta");
		r1klingonen.setAndroidenAnzahl (2);
		r1klingonen.setLebenserhaltungssystemeInProzent (100);
		r1klingonen.setHuelleInProzent (100);
		r1klingonen.setSchildeInProzent (100);
		r1klingonen.setEnergieversorgungInProzent (100);
		r1klingonen.setPhotonentorpedoAnzahl (1);
			
				
		r2romulaner.setSchiffsname ("IRWKhazara");
		r2romulaner.setAndroidenAnzahl (2);
		r2romulaner.setLebenserhaltungssystemeInProzent (100);
		r2romulaner.setHuelleInProzent (100);
		r2romulaner.setSchildeInProzent (100);
		r2romulaner.setEnergieversorgungInProzent (100);
		r2romulaner.setPhotonentorpedoAnzahl (2);
				
		r3vulkanier.setSchiffsname ("Ni`Var");
		r3vulkanier.setAndroidenAnzahl (5);
		r3vulkanier.setLebenserhaltungssystemeInProzent (100);
		r3vulkanier.setHuelleInProzent (100);
		r3vulkanier.setSchildeInProzent (100);
		r3vulkanier.setEnergieversorgungInProzent (100);
		r3vulkanier.setPhotonentorpedoAnzahl (0);
		
		//Ladungen
		
		Ladung l1FerengiSchneckensaft = new Ladung();
		Ladung l2BatlethKlingonenSchwert = new Ladung ();
		Ladung l3BorgSchrott = new Ladung();
		Ladung l4PlasmaWaffe = new Ladung ();
		Ladung l5RoteMaterie = new Ladung();
		Ladung l6Forschungssonde = new Ladung();
		Ladung l7Photonentorpedos = new Ladung ();
		
		l1FerengiSchneckensaft.setBezeichnung("Ferengi Schneckensaft");
		l1FerengiSchneckensaft.setMenge(200);
		
		l2BatlethKlingonenSchwert.setBezeichnung("Bat`leth Klingonen Schwert");
		l2BatlethKlingonenSchwert.setMenge(200);
		
		l3BorgSchrott.setBezeichnung("Borg Schrott");
		l3BorgSchrott.setMenge(5);
		
		l4PlasmaWaffe.setBezeichnung("Plasma-Waffe");
		l4PlasmaWaffe.setMenge(50);
		
		l5RoteMaterie.setBezeichnung("Rote Materie");
		l5RoteMaterie.setMenge(2);
		
		l6Forschungssonde.setBezeichnung("Forschungssonde");
		l6Forschungssonde.setMenge(35);
		
		l7Photonentorpedos.setBezeichnung("Photonentorpedos");
		l7Photonentorpedos.setMenge(3);
		
		
		r1klingonen.addLadung(l1FerengiSchneckensaft);
		r1klingonen.addLadung(l2BatlethKlingonenSchwert);
		
		r1klingonen.zustandRaumschiff();
		
		r2romulaner.addLadung(l3BorgSchrott);
		r2romulaner.addLadung(l5RoteMaterie);
		r2romulaner.addLadung(l4PlasmaWaffe);
		
		r2romulaner.zustandRaumschiff();
		
		r3vulkanier.addLadung(l6Forschungssonde);
		r3vulkanier.addLadung(l7Photonentorpedos);
		
		r3vulkanier.zustandRaumschiff();
		
		klingonen.photonentorpedoSchiessen(vulkanier);
		
	}

}
