import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int androidenAnzahl;
	private int lebenserhaltungssystemeInProzent;
	private int huelleInProzent;
	private int schildeInProzent;
	private int energieversorgungInProzent;
	private int photonentorpedoAnzahl;
	private static ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	// Konstruktor

	public Raumschiff() {
		ladungsverzeichnis = new ArrayList<Ladung>();
		if(broadcastKommunikator == null) {
			this.broadcastKommunikator = new ArrayList();
		}
	}

	public Raumschiff(String schiffsname, int androidenAnzahl, int lebenserhaltungssystemeInProzent,
			int huelleInProzent, int schildeInProzent, int energieversorgungInProzent, int photonentorpedoAnzahl,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladung> ladungsverzeichnis) {
		super();
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.schildeInProzent = schildeInProzent;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		if(broadcastKommunikator == null) {
			this.broadcastKommunikator = new ArrayList();
		}
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	
	// Methoden

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {		
		if (photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl = photonentorpedoAnzahl -1;
			broadcastKommunikator.add("Photonentorpedo abgeschossen");
			treffer(r);
		}
		else System.out.println("-=*Click*=-");
			
	}
	public void phaserkanoneSchiessen(Raumschiff r) {
		
		if(energieversorgungInProzent >50) {
			energieversorgungInProzent = energieversorgungInProzent - 50 ;
			broadcastKommunikator.add("Phaserkanone abgeschossen");
			treffer(r);
		}
		else System.out.println("-=*Click*=-");
		
		
	}
	public void treffer(Raumschiff r) {
		r.setschildeInProzent(r.getschildeInProzent());
		System.out.println(schiffsname + " wurde getroffen!");	
		if (schildeInProzent > 0) {
			schildeInProzent = schildeInProzent -50;
		}
		else 
			huelleInProzent = huelleInProzent -50;
		energieversorgungInProzent = energieversorgungInProzent -50;
		
		if (energieversorgungInProzent == 0) {
			broadcastKommunikator.add("Raumschiff wurde zerst�rt");
		}
			
		
	}
	public void nachtichtAnAlle (String massage) {
		broadcastKommunikator.add(massage);
		
	}
	
	public static ArrayList<String> eintraegeLogbuchZurueckgeben () {
		return broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	
	public void zustandRaumschiff() {
		System.out.println(schiffsname);
		System.out.println(androidenAnzahl);
		System.out.println(lebenserhaltungssystemeInProzent);
		System.out.println(huelleInProzent);
		System.out.println(schildeInProzent);
		System.out.println(energieversorgungInProzent);
		System.out.println(photonentorpedoAnzahl);
		for (int index = 0; index < broadcastKommunikator.size(); index++) {
			System.out.println(broadcastKommunikator.get(index));
		}
		for (int indexx = 0; indexx < ladungsverzeichnis.size(); indexx++) {
			System.out.println(ladungsverzeichnis.get(indexx).bezeichnung);
		}
		
		
	}
	
}
