import java.util.Scanner;

public class whileSchleife {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl Ihrer Wahl ein, um die Quersumme zu berechnen.");
		int eingabewert = tastatur.nextInt();
		int quersumme = 0;
		
		while (eingabewert > 0) {
			quersumme = quersumme + eingabewert%10;
			eingabewert = eingabewert/10;
		}
		System.out.println("Die Quersumme der Zahl ist: " + quersumme);
	}

}
