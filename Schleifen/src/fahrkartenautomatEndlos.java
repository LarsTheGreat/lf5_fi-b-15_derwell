import java.util.Scanner;

class fahrkartenautomatEndlos {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag = 0;
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double r�ckgabebetrag;
		int anzahlFahrkarten;
		double preisEinzelticket = 0;
		int fahrkartenArt;
		
		while (true) {

			System.out.println("Fahrkartenbestellvorgang:");
			
				
			System.out.printf("Sie ihre Wunschfahrkarte f�r Berlin AB aus:\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\nTageskarte Regeltarif AB [8,60 EUR] (2)\n Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n Bezahlen (9)");
				
			fahrkartenArt = tastatur.nextInt();
			System.out.println("Ihre Wahl: "+ fahrkartenArt);
			anzahlFahrkarten = tastatur.nextInt();
			System.out.println("Anzahl der Tickets: " + anzahlFahrkarten);
			
			while (fahrkartenArt != 9) {
				
				while (fahrkartenArt <= 0 | fahrkartenArt >= 4){
					if (fahrkartenArt == 1) {
						preisEinzelticket = 2.90;
						if (fahrkartenArt == 2) {
							preisEinzelticket = 8.60;
							if (fahrkartenArt == 3) {
								preisEinzelticket = 23.5;
							}
						}
					}
				}	
			}
			zuZahlenderBetrag = anzahlFahrkarten * preisEinzelticket;
			System.out.println(zuZahlenderBetrag);
			


			// Geldeinwurf
			// -----------
			eingezahlterGesamtbetrag = 0.0;
			while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
				double betragIns = zuZahlenderBetrag - eingezahlterGesamtbetrag;

				System.out.printf("\nNoch zu zahlen: %.2f Euro \n", betragIns);
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				eingeworfeneM�nze = tastatur.nextDouble();
				eingezahlterGesamtbetrag += eingeworfeneM�nze;
			}

			// Fahrscheinausgabe
			// -----------------
			System.out.println("\nFahrschein wird ausgegeben");
			for (int i = 0; i < 8; i++) {
				System.out.print("=");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("\n\n");

			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------
			r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
			if (r�ckgabebetrag > 0.0) {
				System.out.printf("\nDer R�ckgabebetrag in H�he von %.2f EURO ", r�ckgabebetrag);
				System.out.println("wird in folgenden M�nzen ausgezahlt:");

				while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
				{
					System.out.println("2 EURO");
					r�ckgabebetrag -= 2.0;
				}
				while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
				{
					System.out.println("1 EURO");
					r�ckgabebetrag -= 1.0;
				}
				while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
				{
					System.out.println("50 CENT");
					r�ckgabebetrag -= 0.5;
				}
				while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
				{
					System.out.println("20 CENT");
					r�ckgabebetrag -= 0.2;
				}
				while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
				{
					System.out.println("10 CENT");
					r�ckgabebetrag -= 0.1;
				}
				while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
				{
					System.out.println("5 CENT");
					r�ckgabebetrag -= 0.05;
				}

			}
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
	}
	}

//5
// Ich habe mich f�r einen Int Wert bei der Anzahl der Fahrkarten entscheiden, da man keine halben Fahrkarten kaufen kann
// Und f�r einen Double Wert f�r den Fahrkartenpreis genommen, da dieser auch mit Kommazahlen sein kann.
//6
//Ich Multipliziere den Int Wert anzahlFahrkaten und den Double Wert preisEinzelfahrkarte miteinander um den
//Gesamtpreis (zuZahlenderBetrag) zu berechnen. Dabei kann ich scanne ich den Preis und die Anzahl ein um ein
//dynamisches Programm zuerzeugen.